///<reference path="../../typings/requirejs/require.d.ts" />
///<reference path="../../typings/angularjs/angular.d.ts" />
require([
    'angular'
], function(angular: ng.IAngularStatic) {
    require([
        'controllers/message-controller',
        'services/socket-service'
    ], function(messageController: any, socketService: any) {
        angular
            .module('chat', [])
            .service('socket', socketService)
            .controller('messageController', messageController);
        angular.bootstrap(document, ['chat']);
    });
});
