///<reference path="../../../typings/requirejs/require.d.ts" />
///<reference path="../../../typings/angularjs/angular.d.ts" />

///<reference path="../../shared/message-class.ts" />
//TODO: Dont know why 'Message : typeof MessageClass' works, need some investigation maybe

//TODO: Make a separate service for all logic in controller.
define(['../classes/message-class'], function (Message : typeof MessageClass) {
	return ['socket',
		function (socket: any) {
                var messageVm = this;
                messageVm.messages = [];
                messageVm.username = window.prompt("Представьтесь");
                
                messageVm.message = (author: string, text: string, authorAddress: string) => {
                     messageVm.messages.push(new Message({author, text, authorAddress}));
                }
 
                messageVm.systemMessage = (text: string) => {
                    messageVm.messages.push(new Message({text}))
                }

                socket.on('connecting', () => { messageVm.systemMessage('Соединение...'); });

                socket.on('connect', () => {
                    messageVm.systemMessage('Соединение установлено!');
                    socket.emit("joined", messageVm.username);
                });

                socket.on('message', (data: MessageClass) => {
                    messageVm.message(data.author, data.text, data.authorAddress);
                });
                messageVm.trySubmit = (e: any) => {
                     if (e.which == 13) {
                        messageVm.submit();
                    }
                }
                messageVm.submit = () => {
                    if (messageVm.inputText.length <= 0)
                        return;
                    socket.emit("message", { text: messageVm.inputText, author: messageVm.username });
                    messageVm.inputText = '';
                }

            }
        ];
    });