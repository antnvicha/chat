///<reference path="../../typings/requirejs/require.d.ts" />
require.config({
	paths: {
		angular: '../libs/angular/angular',
		classes:'../classes'
	},
	shim: {
		angular: {
			exports: 'angular'
		}
	},
	deps: ['app']
});
