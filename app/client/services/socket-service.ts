///<reference path="../../../typings/requirejs/require.d.ts" />
///<reference path="../../../typings/angularjs/angular.d.ts" />
///<reference path="../../../typings/socket.io/socket.io-client.d.ts" />

//TODO: Think about better wrapping of socket events.
define([], function () {
    return ['$rootScope', function ($rootScope: ng.IScope) {
        var socket = io.connect(window.location.href);
        return {
            on: function (eventName: string, callback: Function) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName: string, data: any, callback: Function) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            }
        };
    }];
});