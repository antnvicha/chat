///<reference path="../../typings/node/node.d.ts" />
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Chat');

mongoose.connection.on( 'error', (err : Error) => console.log('connection error:', err.message));
mongoose.connection.once('open', ()=> console.log("Connected to DB!"));

module.exports= mongoose;