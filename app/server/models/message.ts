///<reference path="../../../typings/node/node.d.ts" />
///<reference path="../../../typings/mongoose/mongoose.d.ts" />


///<reference path="../../shared/message-class.ts" />
var mongoose = require("../db-config");
var Schema = mongoose.Schema;
var message = new Schema({
    text: {type: String, required: true},
    author: {type: String, required: true},
    authorAddress: {type: String, required: false}
});

var messageModel = mongoose.model('message', message);

module.exports = {
    all: function (callback : Function) {
        messageModel.find(function (err : Error, messages : Array<MessageClass>) {
            if (!err) {
                callback(messages);
            } else {
                console.log("Cant load messages!");
            }
        });
    },
    push: function (author : string, text : string, authorAddress : string) {
        var messageEntity = new messageModel({
            text: text,
            author: author,
            authorAddress: authorAddress
        });
        messageEntity.save(function (err : Error) {
            if (err) console.log("Cant save message");
        });
    }
}