///<reference path="../../typings/node/node.d.ts" />
///<reference path="../../typings/socket.io/socket.io.d.ts" />
///<reference path="../../typings/express/express.d.ts" />

///<reference path="../shared/message-class.ts" />
var Message = require('../shared/message-class')
module.exports = {
    initialize: function (server : Express.Application) {

        var io = require('socket.io').listen(server, {});
        var messageModel = require("./models/message");

        io.sockets.on('connection', function (client: SocketIO.Socket) {
            var address = client.handshake.address;
            client.on('message', function (message : MessageClass) {
                try {
                    message.authorAddress = address;
                    //посылаем сообщение себе
                    client.emit('message', message);
                    //посылаем сообщение всем клиентам, кроме себя
                    client.broadcast.emit('message', message);
                    messageModel.push(message.author, message.text, address);
                } catch (e) {
                    console.log(e);
                    client.disconnect();
                }
            });
            client.on('joined', function (name: string) {
                var message = new Message({author: "Server", text: name + " подключился"});
                client.emit('message', message);
                client.broadcast.emit('message', message);
                messageModel.all((allMessages : Array<MessageClass>) => allMessages.forEach((message : MessageClass)=> client.emit('message', message)));
            });
        });
    }
}
