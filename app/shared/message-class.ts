class MessageClass  {
    //TODO: insert anonymous user as a default user ???
    constructor( {text,author = '',authorAddress='',id = ''} : {id?: string, author?: string, text?: string, authorAddress?: string}) {
        this.id= id;
        this.author = author;
        this.text = text;
        this.authorAddress = authorAddress;
    }
    id : string;
    text : string;
    author : string;
    authorAddress : string;
}

if (typeof (module) == "undefined") {
    define([],function(){
        return MessageClass;
    })
} else {
    module.exports = MessageClass;
}