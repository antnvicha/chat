var gulp = require('gulp');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');

gulp.task('default', ['watch']);

gulp.task('watch', function () {
    gulp.watch('app/**/*.ts', ['compile', ]);
    gulp.watch('server.ts',['entry'])
})

gulp.task('compile', function () {
    return gulp.src('app/**/*.ts')
        .pipe(ts(tsProject))
        .pipe(gulp.dest('app'));
});


gulp.task('entry', function () {
    return gulp.src('server.ts')
        .pipe(ts(tsProject))
        .pipe(gulp.dest('.'));
});