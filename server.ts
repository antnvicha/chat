///<reference path="./typings/node/node.d.ts" />
///<reference path="./typings/socket.io/socket.io.d.ts" />
///<reference path="./typings/express/express.d.ts" />

var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var socket = require("./app/server/socket.io-config");

socket.initialize(server);
server.listen(8080);
console.log("Server started at port 8080");
 
app.use('/public', express.static(__dirname + '/app/client'));
app.use('/libs', express.static(__dirname + '/bower_components'));
app.use('/classes', express.static(__dirname + '/app/shared'));
app.get('/', function (req: Express.Request, res: any) {
    res.sendFile(__dirname + '/app/client/index.html');
});
